#!/usr/bin/python3

from bs4 import BeautifulSoup
import requests
import re
import sys 
import yaml
import argparse
import hashlib
import tempfile
import os
from urllib.parse import urlparse
import zipfile
import tarfile

#TODO license
#TODO test

def extract_info_url(url):
    o = urlparse(url)
    if not o.netloc.endswith('.itch.io'):
        return ('', '')
    if len(o.netloc.split('.')) != 3:
        return ('', '')

    publisher = o.netloc.split('.')[0]
    game_id = o.path.split('/')[1]
    return (publisher, game_id)

def is_csrf_token(tag):
    return tag.has_attr('name') and tag.has_attr('value') and tag.name == 'meta' and tag.attrs['name'] == 'csrf_token'

def get_csrf_token(soup):
    return soup.find_all(is_csrf_token)[0].attrs['value']

#TODO rename
def is_linux_archive(filename):
    d = filename.split('/')
    if len(d) < 3:
        return False
    if d[1] != 'lib':
        return False
    if d[2] == 'linux-x86_64':
        return True


def write_manifest(archive_file, app_id):
    hash_file = hashlib.sha256(open(archive_file, 'rb').read()).hexdigest()

    modules = [
            {   'buildsystem': 'simple',
                'name': 'hello',
                'sources': [ {'path': archive_file,
                              'sha256': hash_file,
                              'type': 'archive',
                              }],
                'build-commands': [
                    'mkdir -p /app/lib/game',
                    'cp -R * /app/lib/game',
                    'rm -f /app/lib/game/*.exe',
                    'rm -Rf /app/lib/game/*.app',
                    'rm -Rf /app/lib/game/lib/darwin-*',
                    'rm -Rf /app/lib/game/lib/windows-*',
                    'rm -Rf /app/lib/game/lib/*-i686',
                    'mkdir -p /app/bin',
                    'echo  \'cd /app/lib/game/; export RENPY_PERFORMANCE_TEST=0; sh *.sh\' > /app/bin/game.sh',
                    'chmod +x /app/bin/game.sh'
                    ],

                }
            ]

    struct = {
            'sdk': 'org.freedesktop.Sdk',
            'runtime': 'org.freedesktop.Platform',
            'runtime-version': '1.6',
            'app-id': app_id,
            'build-options': {'no-debuginfo': True,
                            'strip': False},
            'command': 'game.sh',
            'finish-args': ['--socket=pulseaudio',
                            '--socket=wayland',
                            '--socket=x11',
                            '--device=dri',
                            '--filesystem=~/.renpy'],
            'modules': modules
            }

    with open(app_id + '.yml', 'w') as f:
        f.write(yaml.dump(struct))

def download_file(soup):
    crsf = get_csrf_token(soup)
    for i in soup.select(".download_btn"):
        d = i.attrs['data-upload_id']
        payload = {'csrf_token': crsf}

        r = requests.post(url + '/file/' + d, data=payload)
    
        r = requests.get(r.json()['url'])

        f = r.headers['Content-Disposition']
        fname = re.findall('filename="(.+)"', f)[0]

        with open(fname, 'bw+') as f:
            f.write(r.content)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
            'url',
            help="itch.io url")

    parser.add_argument(
            '--repo',
            help="Flatpak repo.",
            default="repo")

    args = parser.parse_args()

    tmpdir = tempfile.mkdtemp(prefix='renpy2flatpak_')
    os.chdir(tmpdir)

    url = args.url
    repo = args.repo

    (publisher, game_id) = extract_info_url(url)
    if publisher == '':
        print("Wrong url")
        sys.exit(3)

    app_id = 'io.itch.{publisher}.{game_id}'.format(publisher=publisher, game_id=game_id)

    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')



    download_file(soup)
    crsf = get_csrf_token(soup)

    for i in soup.select(".buy_btn"):
        payload = {'csrf_token': crsf}

        r = requests.post(url + '/download_url' , data=payload)
        r = requests.get(r.json()['url'], data=payload)
        s = BeautifulSoup(r.text, 'html.parser')
        download_file(s)

    archive_file = ''
    for i in os.listdir():
        if i.endswith('.zip'):
            with zipfile.ZipFile(i, 'r') as myzip:
                for f in myzip.namelist():
                    if is_linux_archive(f):
                        archive_file = i
        if i.endswith('.tar.bz2'):
            with tarfile.open(i, 'r:bz2') as mytar:
                for f in mytar.getnames():
                    if is_linux_archive(f):
                        archive_file = i


    if archive_file == '':
        print("Unable to find a Linux archive")
        sys.exit(3)

    print(tmpdir)

    write_manifest(archive_file, app_id)

    extra_opt = ''
    if os.environ.get('container','') == 'podman':
        extra_opt = '--disable-rofiles-fuse'
    # TODO sys.Exec
    print('flatpak-builder %s --force-clean build %s.yml --repo %s' % (extra_opt, app_id, repo))

if __name__ == '__main__':
    main()
