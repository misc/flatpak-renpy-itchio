#!/usr/bin/env python3
# coding: utf-8
from setuptools import setup
import os

path = os.path.dirname(__file__)

long_desc = ''
short_desc = 'A script to download renpy game and convert to flatpak'


setup(
    name='flatpak_renpy_itchio',
    description=short_desc,
    long_description=long_desc,
    author='Michael Scherer',
    author_email='misc@zarb.org',
    license='GPLv3+',
    url='https://framagit.org/misc/flatpak-renpy-itchio',
    packages=['flatpak_renpy_itchio'],
    package_data={'': ['COPYING', 'AUTHORS']},
    include_package_data=True,
    version='0.1',
    install_requires=[
        'requests>=1.1.0',
        'beautifulsoup4',
        'pyyaml',
    ],
    setup_requires=["pytest-runner", ],
    tests_require=["pytest", ],
)

